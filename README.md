# Priest Weak Auras
WeakAuras is framework that allows the display of highly customizable graphics on World of Warcraft's user interface to indicate buffs, debuffs, and other relevant information.

This repo contains my weak auras which I use for mixmaxing priest healing

HealbotMissingBuffs - contains weak aura displaying missing buffs of party member in heal bot's group heal bars. Buffs close to expiration start glowing.

Consumables - contains weak aura displaying missing buffs and consumables of YOU, the caster. Buffs & Consumables close to expiration start glowing.

CombatActions - indicates and reminds healer should use Demonic/Dark rune and Major Mana Potion. 